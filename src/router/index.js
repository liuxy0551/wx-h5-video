import Vue from 'vue'
import Router from 'vue-router'
import Layout from '../pages/layout/layout'

Vue.use(Router);

export const constantRouterMap = [
  { path: '/', redirect: '/home' },

  {
    path: '/home',
    component: Layout,
    children: [
      { path: '', name: 'Home', media: { title: '首页' }, component: () => import('../pages/home/index') },
      { path: 'video-article', name: 'VideoArticle', media: { title: '视频文章' }, component: () => import('../pages/home/videoArticle') },
      { path: 'video-full', name: 'VideoFull', media: { title: '全屏播放带浮动操作' }, component: () => import('../pages/home/videoFull') },
    ]
  }
];

export default new Router({
  // mode: 'hash',
  routes: constantRouterMap
})

